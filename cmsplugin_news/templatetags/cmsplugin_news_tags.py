import datetime
from django import template
from cmsplugin_news.models import News

register = template.Library()

@register.inclusion_tag('cmsplugin_news/month_links_snippet.html')
def render_month_links():
    return {
        'dates': News.objects.dates('pub_date', 'month'),
    }