from cms.utils import get_language_from_request
from django.views.generic.list_detail import object_list
from django.views.generic.date_based import object_detail
from models import News

def news_list(request, page):
    lang=get_language_from_request(request)
    qs=News.published.filter(lang=lang)
    return object_list(request, qs, paginate_by=5, page=page)

def news_detail(request, year, month, day, slug):
    lang=get_language_from_request(request)
    qs=News.published.filter(lang=lang)
    return object_detail(request, year, month, day, qs, slug=slug,
        date_field='pub_date', month_format='%m')
