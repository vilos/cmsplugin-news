from django.conf.urls.defaults import patterns
from views import news_detail, news_list


urlpatterns = patterns('',
    (r'^(?:p(?P<page>(?:\d+)|(?:last))/)?$', news_list, {}, 'news_list'),
    (r'^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2}):(?P<slug>[-\w]+)/$',
        news_detail, {}, 'news_detail'),
)