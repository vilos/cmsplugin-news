��          �      <      �  C   �  G   �  K   =     �     �     �  1   �     �  	   �     �            	   +     5     B     G     M  �  e  Z   �  `   A  T   �  	   �       	     )        ;     C     K     g     z     �     �     �     �     �                                  	                                           
                        %(count)d newsitem was published %(count)d newsitems were published %(count)d newsitem was unpublished %(count)d newsitems were unpublished A slug is a short name which uniquely identifies the news item for this day Excerpt Language Latest news Limits the number of items that will be displayed News News item Number of news items to show Publication date Publish selected news Published Read more... Slug Title Unpublish selected news Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2010-09-10 14:26+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n>1 && n<5 ? 1 : 2;
 %(count)d správa zverejnená %(count)d správy zverejnené %(count)d správ zverejnených %(count)d správa zneverejnená %(count)d správy zneverejnené %(count)d správ zneverejnených Identifikátor je krátky názov, ktorý jednoznačne určuje správu pre daný deň Výňatok Jazyk Aktuálne Obmedzí počet správ, ktoré je vidieť Správy Správa Počet viditeľných správ Dátum zverejnenia Zverejniť vybrané správy Zverejnené Viac... Identifikátor Nadpis Zneverejniť vybrané správy 